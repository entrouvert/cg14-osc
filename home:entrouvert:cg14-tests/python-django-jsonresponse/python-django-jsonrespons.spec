Name:           python-django-jsonresponse
Version:        0.9.0
Release:        1%{?dist}
Summary:        Simple wrap django views to render json

Group:          Development/Languages
License:        BSD
URL:            http://pypi.python.org/pypi/django-jsonresponse
Source:         http://pypi.python.org/packages/source/d/django-jsonresponse/django-jsonresponse-%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  python2-devel
BuildRequires:  python-setuptools-devel
Requires:       python-django

%description
Wrap view functions, allowng them render python native and custom objects to json

%prep
%setup -q -n django-jsonresponse-%{version}

%build
%{__python} setup.py build

%install
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT

%files
%{python_sitelib}/jsonresponse
%{python_sitelib}/tests
%doc README.txt PKG-INFO
%{python_sitelib}/django_jsonresponse-%{version}-py?.?.egg-info

%changelog
* Wed Apr 17 2013 Entr'ouvert <info@entrouvert.org> - 0.5-2
- Initial release
