#
# spec file for package python-Mako
#
# Copyright (c) 2012 SUSE LINUX Products GmbH, Nuernberg, Germany.
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via http://bugs.opensuse.org/
#


Name:           python-Mako
Version:        0.7.3
Release:        0
Url:            http://www.makotemplates.org/
Summary:        A super-fast Python templating language
License:        MIT
Group:          Development/Languages/Python
Source:         http://pypi.python.org/packages/source/M/Mako/Mako-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-build
BuildRequires:  python-MarkupSafe
BuildRequires:  python-devel
BuildRequires:  python-distribute
# BuildRequires:  python-nose
Requires:       python-Beaker >= 1.1
Requires:       python-MarkupSafe
Provides:       python-mako = %{version}
Obsoletes:      python-mako < %{version}
%if 0%{?suse_version} && 0%{?suse_version} <= 1110
%{!?python_sitelib: %global python_sitelib %(python -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%else
BuildArch:      noarch
%endif

%description
Mako is a template library written in Python. It provides a familiar, non-XML 
syntax which compiles into Python modules for maximum performance. Mako's 
syntax and API borrows from the best ideas of many others, including Django
templates, Cheetah, Myghty, and Genshi. Conceptually, Mako is an embedded 
Python (i.e. Python Server Page) language, which refines the familiar ideas
of componentized layout and inheritance to produce one of the most 
straightforward and flexible models available, while also maintaining close 
ties to Python calling and scoping semantics.

%prep
%setup -q -n Mako-%{version}

%build
python setup.py build

%install
python setup.py install --prefix=%{_prefix} --root=%{buildroot}

%check
# python setup.py test

%files
%defattr(-,root,root,-)
%doc CHANGES LICENSE README.rst examples
%{_bindir}/mako-render
%{python_sitelib}/mako/
%{python_sitelib}/Mako-%{version}-py%{python_version}.egg-info

%changelog
