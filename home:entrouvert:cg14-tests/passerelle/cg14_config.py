# This file is sourced by "execfile" from passerelle.settings

SECRET_KEY = file('/etc/passerelle/secret').read()

# cg14 root URI is "/passerelle/..."
LOGIN_URL = '/passerelle/accounts/login/'
LOGOUT_URL = '/passerelle/accounts/logout/'
LOGIN_REDIRECT_URL = '/passerelle/manage/'

LANGUAGE_CODE = 'fr-fr'
TIME_ZONE = 'Europe/Paris'

STATIC_ROOT = '/usr/share/passerelle/static'
STATIC_URL = '/passerelle/static/'
MEDIA_ROOT = '/var/lib/passerelle/media'
MEDIA_URL = '/passerelle/media/'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'passerelle',
    }
}

# add solis and makorepost just before gadjo
INSTALLED_APPS = INSTALLED_APPS[:-1] + (
    'solis',
    'makorepost',
    'bdp',
    'gadjo',
    'entrouvert.djommon',
)

# where to find templates for makorepost objects
MAKO_TEMPLATES_DIRS = (
    os.path.join(PACKAGE_PATH, 'apps', 'makorepost', 'templates.mako'),
    os.path.join(PACKAGE_PATH, 'apps', 'solis', 'templates.mako'),
)
MAKO_TEMPLATES_MODULES = None  # store generated Mako module files in this directory (optional)

execfile('/etc/passerelle/config.py')
