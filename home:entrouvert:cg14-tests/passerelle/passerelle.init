#!/bin/sh
#
# passerelle
#
# chkconfig:   2345 20 80
# description: passerelle provides an uniform access to multiple data sources and services.

### BEGIN INIT INFO
# Provides:         passerelle
# Required-Start:   $all
# Required-Stop:    $all
# Should-Start:     $local_fs
# Should-Stop:      $local_fs
# Default-Start:    2 3 4 5
# Default-Stop:     0 1 6
### END INIT INFO

# Source function library.
. /etc/rc.d/init.d/functions

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
NAME=passerelle
DESC="Passerelle (via gunicorn)"
DAEMON="/usr/bin/gunicorn"
SCRIPTNAME=/etc/init.d/$NAME
PIDFILE=/var/run/$NAME/$NAME.pid
LOGFILE=/var/log/$NAME/gunicorn.log
USER=passerelle
GROUP=passerelle
MANAGE=/usr/bin/${NAME}-manage
STARTLOG=/var/log/passerelle/start.log

lockfile=/var/lock/subsys/$NAME

DJANGO_SETTINGS_MODULE=passerelle.settings
DJANGO_CONFIG_FILE=/usr/lib/$NAME/cg14_config.py

DAEMON_OPTIONS="--bind 127.0.0.1:8484 --workers=3"

[ -e /etc/sysconfig/$NAME ] && . /etc/sysconfig/$NAME

do_create_secret() {
    SECRET_FILE=/etc/passerelle/secret
    if [ ! -f $SECRET_FILE ]; then
        echo "Generating Django secret..."
        cat /dev/urandom | tr -dc [:alnum:]-_\!\%\^:\; | head -c70 > $SECRET_FILE
        chown root:passerelle $SECRET_FILE
        chmod 0440 $SECRET_FILE
    fi
}

do_migrate() {
    # install new applications, if any, migrate others if needed
    echo "Installing new apps, applying new migrations (safemigrate)..."
    $SCRIPTNAME manage safemigrate --verbosity=2 --traceback --noinput
}

do_collectstatic() {
    echo "Collect static files..."
    chown -R passerelle:passerelle /usr/share/passerelle/static
    $SCRIPTNAME manage collectstatic --clear --verbosity=2 --traceback --noinput
    chown -R root:root /usr/share/passerelle/static
}

start() {
    [ -x $DAEMON ] || exit 5

    date > $STARTLOG
    do_create_secret >> $STARTLOG 2>&1
    do_migrate >> $STARTLOG 2>&1
    do_collectstatic >> $STARTLOG 2>&1

    echo $"Starting $NAME (via gunicorn): "
    daemon $DAEMON --daemon \
        --pid $PIDFILE \
        --user $USER --group $GROUP \
        --error-logfile $LOGFILE \
        --env DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE \
        --env DJANGO_CONFIG_FILE=$DJANGO_CONFIG_FILE \
        $DAEMON_OPTIONS \
        passerelle.wsgi:application
    retval=$?
    [ $retval -eq 0 ] && touch $lockfile
    return $retval
}


stop() {
    echo -n $"Stopping $NAME: "
    killproc -p $PIDFILE $NAME
    retval=$?
    echo
    [ $retval -eq 0 ] && rm -f $lockfile
    return $retval
}

restart() {
    stop
    start
}

reload() {
    restart
}

force_reload() {
    restart
}

rh_status() {
    # run checks to determine if the service is running or use generic status
    status -p $PIDFILE $NAME
}

rh_status_q() {
    rh_status >/dev/null 2>&1
}


case "$1" in
    start)
        rh_status_q && exit 0
        $1
        ;;
    stop)
        rh_status_q || exit 0
        $1
        ;;
    restart)
        $1
        ;;
    reload)
        rh_status_q || exit 7
        $1
        ;;
    force-reload)
        force_reload
        ;;
    status)
        rh_status
        ;;
    condrestart|try-restart)
        rh_status_q || exit 0
        restart
        ;;
    manage)
        shift
        if [ $(id -un) != $USER ]; then
            sudo -H -u $USER $MANAGE "$@"
        else
            $MANAGE "$@"
        fi
        ;;
    *)
        echo $"Usage: $0 {start|stop|status|restart|condrestart|try-restart|reload|force-reload|manage}"
        exit 2
esac
exit $?

