%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Name:           mod_scgi
Version:        1.13
Release:        6%{?dist}
Summary:        Python implementation of the SCGI protocol

Group:          Applications/Internet
License:        CNRI
URL:            http://python.ca/scgi/
Source0:        http://python.ca/scgi/releases/scgi-%{version}.tar.gz
Source1:        scgi.conf
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  httpd-devel, python-devel, pcre-devel, gcc
Requires:       httpd-mmn = %(cat %{_includedir}/httpd/.mmn || echo missing)
Conflicts:       python-scgi

%description
The SCGI protocol is a replacement for the Common Gateway
Interface (CGI) protocol. It is a standard for applications
to interface with HTTP servers. It is similar to FastCGI
but is designed to be easier to implement.

%prep
%setup -q -n scgi-%{version}


%build
CFLAGS="%{optflags}" %{__python} setup.py build
%{_sbindir}/apxs -c apache2/mod_scgi.c

%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install --root $RPM_BUILD_ROOT 
install -D -m 0755 apache2/.libs/mod_scgi.so  $RPM_BUILD_ROOT/%{_libdir}/httpd/modules/mod_scgi.so
install -D -m 644 %{SOURCE1} $RPM_BUILD_ROOT/%{_sysconfdir}/httpd/conf.d/scgi.conf
mkdir -p $RPM_BUILD_ROOT%{_bindir}
mv $RPM_BUILD_ROOT%{python_sitearch}/scgi/scgi_server.py $RPM_BUILD_ROOT%{_bindir}/scgi_server
mv $RPM_BUILD_ROOT%{python_sitearch}/scgi/quixote_handler.py $RPM_BUILD_ROOT%{_bindir}/quixote_handler


%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt LICENSE.txt README.txt doc/* scgi/test_passfd.py
%config(noreplace) %{_sysconfdir}/httpd/conf.d/scgi.conf
%{python_sitearch}/*egg-info
%{python_sitearch}/scgi
%exclude %{python_sitearch}/scgi/test_passfd.py*
%{_bindir}/scgi_server
%{_bindir}/quixote_handler
%{_libdir}/httpd/modules/mod_scgi.so


%changelog
* Tue Feb 20 2013 Entr'ouvert <info@entrouvert.com> - 1.14-1
- Initial package

