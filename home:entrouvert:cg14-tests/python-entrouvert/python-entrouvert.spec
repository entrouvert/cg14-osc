%{!?python_sitelib: %global python_sitelib %(python -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

%define tag 6.2.0
%define minor 2
%define commit 87131a8

%define version %{tag}.%{minor}.g%{commit}

Name:           python-entrouvert
Version:        %{version}
Release:        0
Summary:        tool modules used in projects of Entr'ouvert
License:        GPLv3
Group:          Development/Languages/Python
Url:            https://dev.entrouvert.org/projects/python-entrouvert/
Source:         python-entrouvert-%{commit}.tar.gz

BuildArch:      noarch
BuildRequires:  python2-devel python-django python-setuptools-devel

%description
This package centralize tool modules used in projects of Entr'ouvert

%prep
%setup -q -n python-entrouvert-%{commit}

%build
%{__python} setup.py build

%install
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT

%files
%doc README
%{python_sitelib}/*
/usr/bin/check-git2python-packaging.sh

%changelog
* Wed Oct 22 2014 Entr'ouvert <info@entrouvert.org> - 0-1
- Initial release

