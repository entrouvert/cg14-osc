Name:           python-django-model-utils
Version:        1.3.1
Release:        1%{?dist}
Summary:        Django model mixins and utilities

Group:          Development/Languages
License:        BSD
URL:            http://pypi.python.org/pypi/django-model-utils
Source:         http://pypi.python.org/packages/source/d/django-model-utils/django-model-utils-%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  python2-devel
BuildRequires:  python-setuptools-devel
Requires:       python-django

%description
Django model mixins and utilities

%prep
%setup -q -n django-model-utils-%{version}

%build
%{__python} setup.py build

%install
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT

%files
%{python_sitelib}/model_utils
%doc AUTHORS.rst CHANGES.rst LICENSE.txt README.rst TODO.rst
%{python_sitelib}/django_model_utils-%{version}-py?.?.egg-info

%changelog
* Wed Apr 17 2013 Entr'ouvert <info@entrouvert.org> - 1.3.1-1
- Initial release
