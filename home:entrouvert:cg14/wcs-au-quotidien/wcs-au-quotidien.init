#!/bin/sh
#
# w.c.s.
#
# chkconfig:   2345 20 80
# description: w.c.s. web form manager

### BEGIN INIT INFO
# Provides:          wcs
# Required-Start:    $local_fs $network $syslog
# Required-Stop:     
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: w.c.s. (Au quotidien)
# Description:       w.c.s. Form Server (with Au Quotidien Extension)
### END INIT INFO

# Source function library.
. /etc/rc.d/init.d/functions

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
DESC="w.c.s. (+Au Quotidien)"
NAME=wcs-au-quotidien
DAEMON=/usr/bin/wcsctl
PIDFILE=/var/run/wcs-au-quotidien/$NAME.pid
SCRIPTNAME=/etc/init.d/$NAME
WCS_USER=wcs-au-quotidien
WCS_GROUP=wcs-au-quotidien
PYTHON_VERSION=`/usr/bin/env python -c \
    "import sys; print '%d.%d' % (sys.version_info[0], sys.version_info[1])"`
OPTIONS="--extra /usr/lib/python$PYTHON_VERSION/site-packages/extra-wcs-au-quotidien/"
CONFIG_FILE=/etc/wcs/wcs-au-quotidien.cfg

lockfile=/var/lock/subsys/$NAME

[ -e /etc/sysconfig/$NAME ] && . /etc/sysconfig/$NAME

start() {
    [ -x $DAEMON ] || exit 5
    [ -f $CONFIG_FILE ] || exit 6
    echo -n $"Starting $NAME: "
    [ x$START_DAEMON != xyes ] && echo " disable by /etc/sysconfig/$NAME" && exit 6
    daemon --user=$WCS_USER $DAEMON -f $CONFIG_FILE start --daemonize --pidfile=$PIDFILE $OPTIONS
    retval=$?
    echo
    [ $retval -eq 0 ] && touch $lockfile
    return $retval
}

stop() {
    echo -n $"Stopping $NAME: "
    killproc -p $PIDFILE $NAME
    retval=$?
    echo
    [ $retval -eq 0 ] && rm -f $lockfile
    return $retval
}

restart() {
    stop
    start
}

reload() {
    restart
}

force_reload() {
    restart
}

rh_status() {
    # run checks to determine if the service is running or use generic status
    status -p $PIDFILE $NAME
}

rh_status_q() {
    rh_status >/dev/null 2>&1
}


case "$1" in
    start)
        rh_status_q && exit 0
        $1
        ;;
    stop)
        rh_status_q || exit 0
        $1
        ;;
    restart)
        $1
        ;;
    reload)
        rh_status_q || exit 7
        $1
        ;;
    force-reload)
        force_reload
        ;;
    status)
        rh_status
        ;;
    condrestart|try-restart)
        rh_status_q || exit 0
        restart
        ;;
    *)
        echo $"Usage: $0 {start|stop|status|restart|condrestart|try-restart|reload|force-reload}"
        exit 2
esac
exit $?
