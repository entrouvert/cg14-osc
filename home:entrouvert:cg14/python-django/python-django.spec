#
# spec file for package python-django
#
# Copyright (c) 2013 SUSE LINUX Products GmbH, Nuernberg, Germany.
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via http://bugs.opensuse.org/
#


Name:           python-django
Version:        1.5.10
Release:        0
Summary:        A high-level Python Web framework
License:        BSD-3-Clause
Group:          Development/Languages/Python
Url:            http://www.djangoproject.com
Source:         Django-%{version}.tar.gz
BuildRequires:  python-devel
BuildRoot:      %{_tmppath}/%{name}-%{version}-build
#Requires:       gettext-tools
%if 0%{?suse_version}
%py_requires
%if 0%{?suse_version} > 1110
BuildArch:      noarch
%endif
%endif
%{!?python_sitelib: %global python_sitelib %(python -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

%description
Django is a high-level Python Web framework that encourages rapid development and clean, pragmatic design.

%prep
%setup -q -n Django-%{version}
# %patch1
# %patch2

%build
python setup.py build

%install
python setup.py install --prefix=%{_prefix} --root=%{buildroot}
install -D -m 0755 extras/django_bash_completion %{buildroot}%{_sysconfdir}/bash_completion.d/django_bash_completion.sh
ln -s %{_bindir}/django-admin.py %{buildroot}%{_bindir}/django-admin

%files
%defattr(-,root,root,-)
%doc AUTHORS LICENSE README.rst
%{_bindir}/django-admin*
%{python_sitelib}/*
%{_sysconfdir}/bash_completion.d/django_bash_completion.sh

%changelog
