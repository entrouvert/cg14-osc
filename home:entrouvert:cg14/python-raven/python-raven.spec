Name:           python-raven
Version:        3.4.1
Release:        1%{?dist}
Summary:        Raven is a client for Sentry (https://www.getsentry.com)

Group:          Development/Languages
License:        BSD
URL:            http://pypi.python.org/pypi/raven
Source:         https://pypi.python.org/packages/source/r/raven/raven-%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  python2-devel
BuildRequires:  python-setuptools-devel

%description
Raven is a Python client for Sentry http://getsentry.com/. It provides full
out-of-the-box support for many of the popular frameworks, including Django
http://djangoproject.com, Flask http://flask.pocoo.org/ and Pylons
http://www.pylonsproject.org/. Raven also includes drop-in support for any
WSGI-compatible web application.

%prep
%setup -q -n raven-%{version}

%build
%{__python} setup.py build

%install
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT

%files
%{python_sitelib}/raven
%{_bindir}/raven
%doc README.rst PKG-INFO
%{python_sitelib}/raven-%{version}-py?.?.egg-info

