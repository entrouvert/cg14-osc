%{!?python_sitelib: %define python_sitelib %(python -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

%define tag 1.12.5
%define commit 4ee347a
# si possible, minor = nombre de commit depuis le tag (à la "git describe")
%define minor 42

# si minor == 0
#%define version %{tag}
# sinon :
%define version %{tag}.%{minor}.g%{commit}

Name:           wcs
Version:        %{version}
Release:        9%{?dist}
Summary:        web application to design and set up online forms

Group:          Development/Libraries
License:        GPLv2
URL:            http://www.entrouvert.com/fr/e-administration/wcs/
Source0:        http://repos.entrouvert.org/wcs.git/snapshot/wcs-%{commit}.tar.gz
Source1:        wcs.init
Source2:        wcs.sysconf
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  python-devel python-quixote make gettext
Requires:       mod_scgi python-quixote python-raven
Requires(pre):  shadow-utils
Requires(post): /sbin/chkconfig
Requires(preun): /sbin/service, /sbin/chkconfig
Requires(postun): /sbin/service


%description
w.c.s. is a web application which allows to design and set up online forms. 
It gives a user the ability to create web forms easily without requiring any
other skill than familiarity with web surfing

%prep
%setup -q -n wcs-%{commit}

%pre
getent group wcs >/dev/null || groupadd -r wcs
getent passwd wcs >/dev/null || \
    useradd -r -g wcs -d /var/lib/wcs -s /sbin/nologin \
    -c "w.c.s software user" wcs
exit 0

%build
CFLAGS="$RPM_OPT_FLAGS" %{__python} setup.py build


%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT
install -d $RPM_BUILD_ROOT/var/lib/wcs
install -d $RPM_BUILD_ROOT/etc/wcs
install -m 644 wcs.cfg-sample $RPM_BUILD_ROOT/etc/wcs/wcs.cfg
install -D -p -m 0755 %{SOURCE1} \
    %{buildroot}%{_sysconfdir}/rc.d/init.d/wcs
install -D -p -m 644 %{SOURCE2} \
    $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/wcs
mv %{buildroot}%{_bindir}/wcsctl.py %{buildroot}%{_bindir}/wcsctl
mkdir -p %{buildroot}/var/run/wcs
cd po && make install prefix=$RPM_BUILD_ROOT

%post
/sbin/chkconfig --add wcs

%preun
if [ $1 -eq 0 ]; then
    /sbin/service wcs stop &>/dev/null || :
    /sbin/chkconfig --del wcs
fi

%postun
if [ $1 -ge 1 ]; then
    /sbin/service wcs condrestart &>/dev/null || :
fi


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc doc/ COPYING NEWS README
%{python_sitelib}/wcs*
%{_bindir}/wcsctl
%{_datadir}/wcs*
/usr/share/locale*
%{_sysconfdir}/rc.d/init.d/wcs
%config(noreplace) /etc/wcs/wcs.cfg
%config(noreplace) %{_sysconfdir}/sysconfig/*
%defattr(-,wcs,wcs,-)
%dir /var/lib/wcs
%dir /var/run/wcs

%changelog
