# Configuration for passerelle.
# You can override Passerelle default settings here

# Passerelle is a Django application: for the full list of settings and their
# values, see https://docs.djangoproject.com/en/1.7/ref/settings/
# For more information on settings see
# https://docs.djangoproject.com/en/1.7/topics/settings/
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# This file is sourced by "execfile" from /usr/lib/passerelle/cg14_config.py

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False
TEMPLATE_DEBUG = False

# mailto for fatal errors (500)
ADMINS = (
        ('admin passerelle', 'passerelle-admin@calvados.fr'),
        # ('User 1', 'watchdog@example.net'),
        # ('User 2', 'janitor@example.net'),
)
MANAGERS = ADMINS

# ALLOWED_HOSTS must be correct in production
ALLOWED_HOSTS = ['*']   # cg14: we are behind a pile of reverse proxies... :/
#See https://docs.djangoproject.com/en/dev/ref/settings/#allowed-hosts
#ALLOWED_HOSTS = [
#        'teleservices.calvados.fr',
#        'teleservices.calvados.fr.',
#        'internal-reverse-proxy-name',
#        'internal-reverse-proxy-name.',
#]

# Passerelle use a postgresql database
# Default: a local database named "passerelle"
# DATABASES['default']['name'] = 'passerelle'
# DATABASES['default']['user'] = 'passerelle'
# DATABASES['default']['password'] = '******'
# DATABASES['default']['host'] = 'localhost'
# DATABASES['default']['port'] = '5432'

