Name:           python-django-jsonfield
Version:        0.9.13
Release:        1%{?dist}
Summary:        JSONField for django models

Group:          Development/Languages
License:        BSD
URL:            http://pypi.python.org/pypi/django-jsonfield
Source:         http://pypi.python.org/packages/source/d/django-jsonfield/django-jsonfield-%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  python2-devel
BuildRequires:  python-setuptools-devel
Requires:       python-django

%description
JSONField for django models

%prep
%setup -q -n django-jsonfield-%{version}

%build
%{__python} setup.py build

%install
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT

%files
%{python_sitelib}/jsonfield
%doc LICENSE README.rst 
%{python_sitelib}/django_jsonfield-%{version}-py?.?.egg-info

%changelog
* Sat Oct 18 2014 Entr'ouvert <info@entrouvert.org> - 0.9.13-1
- Initial release
