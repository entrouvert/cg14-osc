%define tag 0.2
%define commit 3425188
# si possible, minor = nombre de commit depuis le tag (à la "git describe")
%define minor 22

# si minor == 0 
#%define version %{tag}
# sinon : 
%define version %{tag}.%{minor}.g%{commit}

# %define _unpackaged_files_terminate_build 0

Name:           python-django-gadjo
Version:        %{tag}.%{minor}.g%{commit}
Release:        9%{?dist}
Summary:        Django base template tailored for management interfaces

Group:          Development/Languages
License:        AGPLv3
URL:            https://dev.entrouvert.org/projects/gadjo
Source0:        gadjo-%{commit}.tar.gz

BuildArch:      noarch
BuildRequires:  python2-devel python-django python-setuptools-devel
Requires:       python-django python-XStatic python-XStatic-Font-Awesome python-XStatic-jQuery python-XStatic-jquery-ui

%description
Gadjo is a base template for Django applications, tailored for
management interfaces, built to provide a nice and modern look, while
using progressive enhancement and responsive designs to adapt to
different environments.

%prep
%setup -q -n gadjo-%{commit}

%build
%{__python} setup.py build

%install
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT

%files
%{python_sitelib}/gadjo
%{python_sitelib}/gadjo-*.egg-info
%doc COPYING README.txt AUTHORS

%changelog
* Sat Oct 18 2014 Entr'ouvert <info@entrouvert.org> - 0-1
- Initial release
