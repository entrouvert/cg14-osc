%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

%define srcname Quixote

Name:           python-quixote
Version:        2.7
Release:        6%{?dist}
Summary:        A highly Pythonic Web application framework

Group:          Development/Libraries
License:        CNRI
URL:            http://quixote.ca/
Source0:        http://quixote.python.ca/releases/%{srcname}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  python-devel gcc

%description
Quixote is a framework for developing Web applications in Python. Quixote is
based on a simple, flexible design, making it possible to write applications
quickly and to benefit from the wide range of available third-party Python
modules. Deployed appropriately, Quixote has excellent performance that allows
you to put Quixote-based applications into large-scale production use.

%prep
%setup -q -n %{srcname}-%{version}


%build
CFLAGS="$RPM_OPT_FLAGS" %{__python} setup.py build


%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc doc/ CHANGES.txt README.txt TODO LICENSE.txt
%dir %{python_sitearch}/quixote
%{python_sitearch}/quixote/*
%{python_sitearch}/Quixote-%{version}-py?.?.egg-info

%changelog
* Tue Feb 19 2013 Entr'ouvert <info@entrouvert.com> - 2.7-1
- Initial package
