#
# spec file for package python-MarkupSafe
#
# Copyright (c) 2012 SUSE LINUX Products GmbH, Nuernberg, Germany.
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via http://bugs.opensuse.org/
#


Name:           python-MarkupSafe
Version:        0.15
Release:        1
Url:            http://dev.pocoo.org/
Summary:        Implements a XML/HTML/XHTML Markup safe string for Python
License:        BSD-3-Clause
Group:          Development/Languages/Python
Source:         http://pypi.python.org/packages/source/M/MarkupSafe/MarkupSafe-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-build
BuildRequires:  python-devel
BuildRequires:  python-distribute
%if 0%{?suse_version}
%py_requires
%endif
Provides:       python-markupsafe = %{version}
Obsoletes:      python-markupsafe < %{version}
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

%description
Implements a unicode subclass that supports HTML strings. This can be used to
safely encode strings for dynamically generated web pages.

%prep
%setup -q -n MarkupSafe-%{version}

%build
CFLAGS="%{optflags}" python setup.py build

%install
python setup.py install --prefix=%{_prefix} --root=%{buildroot}

%check
python setup.py test

%files
%defattr(-,root,root,-)
%doc AUTHORS LICENSE README.rst
%{python_sitearch}/markupsafe/
%{python_sitearch}/MarkupSafe-%{version}-py%{python_version}.egg-info

%changelog
