%{!?python_sitelib: %define python_sitelib %(python -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

%define tag 1.12.12
%define commit 2f51ce8
# si possible, minor = nombre de commit depuis le tag (à la "git describe")
%define minor 0

# si minor == 0 
#%define version %{tag}
# sinon : 
%define version %{tag}.%{minor}.g%{commit}

Name:           wcs-au-quotidien
Version:        %{version}
Release:        2
Summary:        w.c.s. Form Server (Au Quotidien Extension)

Group:          Development/Libraries
License:        GPLv2
URL:            http://www.entrouvert.com/fr/e-administration/au-quotidien/
Source0:        http://repos.entrouvert.org/auquotidien.git/snapshot/auquotidien-%{commit}.tar.gz
Source1:        wcs-au-quotidien.init
Source2:        wcs-au-quotidien.sysconf
Source3:        wcs-cg14-tool
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:	    noarch
BuildRequires:  python-devel python-quixote make gettext
Requires:       python-vobject wcs
Requires(pre):  shadow-utils
Requires(post): /sbin/chkconfig
Requires(preun): /sbin/service, /sbin/chkconfig
Requires(postun): /sbin/service

%description
w.c.s. Form Server (Au Quotidien Extension)

%prep
%setup -q -n auquotidien-%{commit}

%pre
getent group wcs-au-quotidien >/dev/null || groupadd -r wcs-au-quotidien
getent passwd wcs-au-quotidien >/dev/null || \
    useradd -r -g wcs-au-quotidien -d /var/lib/wcs-au-quotidien -s /sbin/nologin \
    -c "w.c.s. au quotidien software user" wcs-au-quotidien
exit 0

%build
CFLAGS="$RPM_OPT_FLAGS" %{__python} setup.py build


%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT
install -d $RPM_BUILD_ROOT/var/lib/wcs-au-quotidien
install -d $RPM_BUILD_ROOT/etc/wcs
install -m 644 wcs-au-quotidien.cfg-sample $RPM_BUILD_ROOT/etc/wcs/wcs-au-quotidien.cfg
install -D -p -m 0755 %{SOURCE1} \
    %{buildroot}%{_sysconfdir}/rc.d/init.d/wcs-au-quotidien
install -D -p -m 644 %{SOURCE2} \
    $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/wcs-au-quotidien
mkdir -p %{buildroot}/var/run/wcs-au-quotidien
for i in $RPM_BUILD_ROOT/usr/lib/python2.*; do \
        mv $i/site-packages/extra \
        $i/site-packages/extra-wcs-au-quotidien; done
cd po && make install prefix=$RPM_BUILD_ROOT
install -d $RPM_BUILD_ROOT/usr/local/bin
install -D -p -m 0755 %{SOURCE3} $RPM_BUILD_ROOT/usr/local/bin/wcs-cg14-tool

%post
/sbin/chkconfig --add wcs-au-quotidien

%preun
if [ $1 -eq 0 ]; then
    /sbin/service wcs-au-quotidien stop &>/dev/null || :
    /sbin/chkconfig --del wcs-au-quotidien
fi

%postun
if [ $1 -ge 1 ]; then
    /sbin/service wcs-au-quotidien condrestart &>/dev/null || :
fi


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc README
%{python_sitelib}/extra-wcs-au-quotidien*
%{python_sitelib}/wcs_au_quotidien-*.egg-info
%{_datadir}/wcs*
%{_datadir}/auquotidien*
/usr/share/locale*
/usr/local/bin/wcs*
%{_sysconfdir}/rc.d/init.d/wcs-au-quotidien
%config(noreplace) /etc/wcs/wcs-au-quotidien.cfg
%config(noreplace) %{_sysconfdir}/sysconfig/*
%defattr(-,wcs-au-quotidien,wcs-au-quotidien,-)
%dir /var/lib/wcs-au-quotidien
%dir /var/run/wcs-au-quotidien

%changelog
* Tue Feb 20 2013 Entr'ouvert <info@entrouvert.com> - 20130205+2e7cace-1
- Initial package
