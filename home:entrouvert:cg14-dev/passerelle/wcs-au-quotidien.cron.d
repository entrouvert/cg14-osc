SHELL=/bin/bash
PATH=/sbin:/bin:/usr/sbin:/usr/bin
MAILTO=jean-damien.bouvier@calvados.fr
HOME=/

# For details see man 4 crontabs

# Example of job definition:
# .---------------- minute (0 - 59)
# |  .------------- hour (0 - 23)
# |  |  .---------- day of month (1 - 31)
# |  |  |  .------- month (1 - 12) OR jan,feb,mar,apr ...
# |  |  |  |  .---- day of week (0 - 6) (Sunday=0 or 7) OR sun,mon,tue,wed,thu,fri,sat
# |  |  |  |  |
# *  *  *  *  * user-name command to be executed

# production
# 10,30,50 * * * * wcs-au-quotidien /etc/passerelle/synchro-cron.sh

# test/recette
# 0,5,10,15,20,25,30,35,40,45,50,55 * * * * wcs-au-quotidien /etc/passerelle/synchro-cron.sh

