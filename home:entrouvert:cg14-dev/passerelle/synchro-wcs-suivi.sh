#!/bin/bash
set -e

if [ `/usr/bin/id -un` != 'wcs-au-quotidien' ]
then
	echo "$0 must be run by user wcs-au-quotidien"
	exit 1
fi

BASE_URL=https://teleservices.calvados.fr/passerelle/ 
APIKEY=d24709cfde8b9e9b5743c43c3ab49f65
VHOST=apl108

if [ -r /etc/passerelle/synchro-wcs-suivi.conf ]
then
    . /etc/passerelle/synchro-wcs-suivi.conf
fi

CURL=/usr/bin/curl

WCSCTL_OPTS="--vhost=$VHOST --app-dir=/var/lib/wcs-au-quotidien"

mkdir -p /var/tmp/synchro-wcs-solis
cd /var/tmp/synchro-wcs-solis

###
### End of configuration
###

if [ ! -x $CURL ]
then
	echo "cURL not found ($CURL)"
	exit 1
fi

if [ $# -lt 1 ]
then
	echo "syntax: $0 <triggername>"
	exit 1
fi

TRIGGER=$1

URL="${BASE_URL}repost/${TRIGGER}/json?apikey=${APIKEY}"
output=${TRIGGER}.json

$CURL --max-time 20 --insecure -sS -o ${output}.raw "$URL" || exit

# JSON analyse
python << EOP > $output
import json
import sys
output = json.load(file('${output}.raw'))

data = output['data']
err = output['err']

if err == 0 and data is not None:
    print json.dumps(data, indent=4)
else:
    print >> sys.stderr, 'err:', err
    print >> sys.stderr, 'err_class:', output.get('err_class')
    print >> sys.stderr, 'err_desc:', output.get('err_desc')
    sys.exit(1)
EOP

#echo "trigger $TRIGGER"
wcsctl trigger-jumps ${WCSCTL_OPTS} --trigger=${TRIGGER} ${output}

