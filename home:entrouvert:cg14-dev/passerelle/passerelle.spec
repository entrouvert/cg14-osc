
%define tag 0.5.0
%define commit ef79c2a
# si possible, minor = nombre de commit depuis le tag (à la "git describe")
%define minor 48

# si minor == 0 
#%define version %{tag}
# sinon : 
%define version %{tag}.%{minor}.g%{commit}

%define _unpackaged_files_terminate_build 0

Name:           passerelle
Version:        %{tag}.%{minor}.g%{commit}
Release:        9%{?dist}
Summary:        Uniform access to multiple data sources and services.

Group:          Development/Languages
License:        AGPL
URL:            https://dev.entrouvert.org/projects/passerelle
Source0:        passerelle-%{commit}.tar.gz
Source1:        passerelle.init
Source2:        config.py
Source3:        synchro-wcs-suivi.sh
Source4:        synchro-wcs-suivi.conf
Source5:        synchro-cron.sh
Source6:        wcs-au-quotidien.cron.d
Source7:        passerelle-manage
Source8:        cg14_config.py
Patch1:         0001-bdp-add-parameters-to-post-adherent-6577.patch


BuildArch:      noarch
BuildRequires:  python2-devel python-django python-setuptools-devel
Requires:       python-django python-South python-django-jsonresponse python-django-model-utils python-gunicorn python-Mako python-raven python-django-gadjo python-requests python-django-jsonfield python-entrouvert

%description
Passerelle provides an uniform access to multiple data sources and services.

%prep
%setup -q -n passerelle-%{commit}
%patch1 -p1

%pre
getent group passerelle >/dev/null || groupadd -r passerelle
getent passwd passerelle >/dev/null || useradd -r -g passerelle -s /sbin/nologin -c "passerelle software user" passerelle
install -d -m 0750 -o passerelle -g passerelle /var/run/passerelle
install -d -m 0750 -o passerelle -g passerelle /var/log/passerelle
install -d -m 0750 -o passerelle -g passerelle /var/lib/passerelle/media
install -d -m 0750 -o passerelle -g passerelle /usr/share/lib/passerelle/static
chown -R passerelle:passerelle /usr/share/lib/passerelle/static
exit 0

%build
%{__python} setup.py build

%install
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT
install -d -m 0755 $RPM_BUILD_ROOT/usr/lib/passerelle
mv $RPM_BUILD_ROOT/usr/bin/manage.py $RPM_BUILD_ROOT/usr/lib/passerelle/manage.py
install -D -p -m 755 %{SOURCE8} $RPM_BUILD_ROOT/usr/lib/passerelle/cg14_config.py
install -D -p -m 755 %{SOURCE7} $RPM_BUILD_ROOT/usr/bin/passerelle-manage
install -d -m 0755 $RPM_BUILD_ROOT/etc/passerelle
install -D -p -m 644 %{SOURCE2} $RPM_BUILD_ROOT/etc/passerelle/config.py
install -D -p -m 755 %{SOURCE3} $RPM_BUILD_ROOT/etc/passerelle/synchro-wcs-suivi.sh
install -D -p -m 644 %{SOURCE4} $RPM_BUILD_ROOT/etc/passerelle/synchro-wcs-suivi.conf
install -D -p -m 755 %{SOURCE5} $RPM_BUILD_ROOT/etc/passerelle/synchro-cron.sh
install -D -p -m 644 %{SOURCE6} $RPM_BUILD_ROOT/etc/cron.d/wcs-au-quotidien
install -D -p -m 0755 %{SOURCE1} %{buildroot}%{_sysconfdir}/rc.d/init.d/passerelle
install -d -m 0755 $RPM_BUILD_ROOT/usr/share/passerelle/templates.mako

%post
/sbin/chkconfig --add passerelle

%preun
if [ $1 -eq 0 ]; then
    /sbin/service passerelle stop &>/dev/null || :
    /sbin/chkconfig --del passerelle
fi

%postun
if [ $1 -ge 1 ]; then
    /sbin/service passerelle condrestart &>/dev/null || :
fi

%clean
rm -rf $RPM_BUILD_ROOT

%files
%{python_sitelib}/passerelle
%{python_sitelib}/passerelle-*.egg-info
%{_sysconfdir}/rc.d/init.d/passerelle
/usr/bin/passerelle-manage
/usr/lib/passerelle/manage.py
/usr/lib/passerelle/cg14_config.py
/etc/passerelle/synchro-cron.sh
/etc/passerelle/synchro-wcs-suivi.sh
%config /etc/passerelle/config.py
%config /etc/passerelle/synchro-wcs-suivi.conf
%config /etc/cron.d/wcs-au-quotidien
%doc LICENSE

%changelog
* Wed Apr 17 2013 Entr'ouvert <info@entrouvert.org> - 0-1
- Initial release
