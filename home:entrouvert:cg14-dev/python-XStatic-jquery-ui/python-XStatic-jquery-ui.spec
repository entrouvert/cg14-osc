%global pypi_name XStatic-jquery-ui

%{!?__python2:%global __python2 %{__python}}
%{!?python2_sitelib:   %global python2_sitelib         %{python_sitelib}}
%{!?python2_sitearch:  %global python2_sitearch        %{python_sitearch}}
%{!?python2_version:   %global python2_version         %{python_version}}

Name:           python-%{pypi_name}
Version:        1.11.0.1
Release:        2%{?dist}
Summary:        jquery-ui (XStatic packaging standard)

# According 
# https://fedoraproject.org/wiki/Licensing:Main?rd=Licensing#Good_Licenses
# http://creativecommons.org/publicdomain/zero/1.0/legalcode
# is abbreviated CCO.
# This package has the same license as jquery-ui:
# https://github.com/jquery/jqueryui.com/blob/master/LICENSE.txt
License:        CCO
URL:            http://jqueryui.com/
Source0:        https://pypi.python.org/packages/source/X/%{pypi_name}/%{pypi_name}-%{version}.tar.gz
BuildArch:      noarch
 
BuildRequires:  python-devel
BuildRequires:  python-setuptools

Requires: python-XStatic

%description
jquery-ui javascript library packaged for
setuptools (easy_install) / pip.

This package is intended to be used by
**any** project that needs these files.

It intentionally does **not** provide
any extra code except some metadata
**nor** has any extra requirements. You MAY
use some minimal support code from
the XStatic base package, if you like.

%prep
%setup -q -n %{pypi_name}-%{version}
# Remove bundled egg-info
rm -rf %{pypi_name}.egg-info

%build
# due https://bitbucket.org/thomaswaldmann/xstatic/issue/2/
# this package can not be built with python-XStatic installed.
%{__python2} setup.py build

%install
%{__python2} setup.py install --skip-build --root %{buildroot}

%files
%doc README.txt
%{python2_sitelib}/XStatic_jquery_ui-1.11.0.1-py%{python_version}.egg-info
%{python2_sitelib}/XStatic_jquery_ui-1.11.0.1-py%{python_version}-nspkg.pth
%{python2_sitelib}/xstatic/pkg/jquery_ui

%changelog
* Mon Oct 20 2014 Thomas NOEL <info@entrouvert.com> - 1.11.0.1-1
- update to 1.11.0.1
* Mon Oct 20 2014 EO <info@entrouvert.com> - 1.10.4.1-2
- dont use _jsdir
* Mon Aug 18 2014 Matthias Runge <mrunge@redhat.com> - 1.10.4.1-1
- Initial package (rhbz#1135430).
